<?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>
            <?php echo 'Patrologia Graeca Search' ?>:
            <?php echo $title_for_layout; ?>
        </title>
        <?php
        echo $this->Html->meta('icon');

        echo $this->Html->css('cake.generic');
        echo $this->Html->css('../jquery-ui-1.8.21.custom/css/ui-lightness/jquery-ui-1.8.21.custom.css"');
        echo $this->Html->css('../jquery.tablesorter/themes/blue/style.css');
        echo $this->Html->css('../jquery.tablesorter/themes/style.css');
        echo $this->Html->css('../bootstrap/css/bootstrap.css');
        echo $this->Html->css('../css/template.css');

        echo $this->Html->script('../bootstrap/js/jquery.js');
        echo $this->Html->script('../bootstrap/js/bootstrap-dropdown.js');
        echo $this->Html->script('../jquery.tablesorter/jquery.tablesorter.js');
        echo $this->Html->script('../jquery.tablesorter/tablesorter.widgets.js');
        echo $this->Html->script('../jquery-ui-1.8.21.custom//js/jquery-ui-1.8.21.custom.min.js');
        echo $this->Html->script('../jquery.tablesorter/tablesorter_filter.js');
        echo $this->Html->script('../jquery.tablesorter/addons/pager/jquery.tablesorter.pager.js');


        echo $this->fetch('meta');
        echo $this->fetch('css');
        echo $this->fetch('script');
        ?>

    </head>
    <body>

        <div id="header">
            <div id="appName">
                <div id="textAppName">

                    Patrologia Graeca Search</div>

            </div>
            <div class="actions">
                <ul>
                    <li><?php echo $this->Html->link(__('Return Home'), 'http://patrologiagraeca.org/patrologia/'); ?></li>
                    <li><?php echo $this->Html->link(__('Quick Search'), array('action' => 'index')); ?></li>
                </ul>
                
                <div id="imageDiv"><?php echo $this->Html->image('logo.png', array('align' => 'top')); ?></div>

            </div> 
            <div id="share">
<a href="http://www.facebook.com/sharer.php?u=http://patrologiagraeca.org/patrologia/patrologiasearch/books&t=Quick Search of Authors of Patrologia Graeca J.-P.Migne" target="_blank"> <?php echo $this->Html->image('facebook_icon.png', array('align' => 'top')); ?></a>    
<a href="http://twitter.com/intent/tweet?text=Quick Search of Authors of Patrologia Graeca J.-P.Migne&url=http://patrologiagraeca.org/patrologia/patrologiasearch/books" title=”Click to share this post on Twitter”>twit</a>

            </div>


        </div>
        <div id="container">

            <div id="content">

                <?php echo $this->Session->flash(); ?>

                <?php echo $this->fetch('content'); ?>
            </div>

        </div>
    </body>
</html>
