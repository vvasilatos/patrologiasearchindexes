<rsp stat="ok"> 
<books type='array'> 
    <?php foreach ($books as $book) : ?> 
        <book type='struct'> 
            <number><?php echo $book['Book']['number'];?></number> 
            <writers><?php echo $book['Book']['writers'];?></writers>
            <writersen><?php echo $book['Book']['writersen'];?></writersen>
            <century><?php echo $book['Book']['century'];?></century> 
            
        </book> 
    <?php endforeach; ?> 
</books>
</rsp> 
