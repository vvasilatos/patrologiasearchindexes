<div class="books index">
    <div id="searchInTheTable" class="ui-widget">
        <label for="tags"><i class="icon-search"></i> Search: </label> <input id="tags" placeholder="type here...">
    </div>
    <h2><?php echo __('Κατάλογος συγγραφέων P.G. J.-P.Migne'); ?></h2>
    <table id="myTable" class="tablesorter" style="border:thin #000000;">
        <thead>
            <tr>
                <th><?php echo 'Τόμος/Volume'; ?></th>
                <th><?php echo 'Αιώνας/Century'; ?></th>
                <th colspan="2"><?php echo 'Συγγραφείς/Writers'; ?></th>

            </tr>
        </thead>
        <tbody>
            <?php foreach ($books as $book): ?>
                <tr>
                    <td><?php echo h($book['Book']['number']); ?>&nbsp;</td>
                    <td><?php echo h($book['Book']['century']); ?>&nbsp;</td>
                    <td><?php echo h($book['Book']['writers']); ?>&nbsp;</td>
                    <td><?php echo h($book['Book']['writersen']); ?>&nbsp;</td>
                </tr>

            <?php endforeach; ?>
        </tbody>
    </table>
    <script type="text/javascript">
        jQuery(document).ready(function() {
            $("#myTable")
            .tablesorter({debug: false, widgets: ['zebra'], sortList: [[0,0]]})
            .tablesorterFilter({filterContainer: $("#tags"),
                filterClearContainer: $("#filter-clear-button"),
                filterColumns: [0,1,2,3],
                filterCaseSensitive: false});
        });
    </script>

    
</div>



